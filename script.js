$( document ).ready(function() {


var stendingsItaly = new Stendings("SA", "#italy")
stendingsItaly.getData()

var stendingsGermany = new Stendings("BL1", "#germany")
stendingsGermany.getData()

var stendingsEngland = new Stendings("PL", "#england")
stendingsEngland.getData()

var stendingsIspani = new Stendings("PD", "#ispani")
stendingsIspani.getData()

var stendingsFrance = new Stendings("FL1", "#france")
stendingsFrance.getData()



})




function Stendings(teamCode,container){
	self = this,
	this.teamCode = teamCode,
	this.container = container,
	header = {
		key: "X-Auth-Token",
		value: "34d22f1f21104d69b5f427d6ec66fb1f"
	},
	method = "GET",
	server = "https://api.football-data.org/v2/"

	this.drow = function(info){
		var tableTitile = '<div class="container">'+
    	'<p>'+ info.competition.area.name + ' arajnutyun : ' + info.season.startDate + '/' + info.season.endDate + '</p>'+
    	'<table class=table> <thead> <tr>' +
    	'<th>команда</th>'+
    	'<th>И</th>'+
    	'<th>В</th>'+
    	'<th>Н</th>'+
    	'<th>П</th>'+
    	'<th>Г</th>'+
    	'<th>О</th>   </tr> </thead> <tbody>' ;

    	info.standings[0].table.forEach(function(item){
    		var name = item.team.name.replace("Juventus FC","Ювентус").replace("SSC Napoli","Наполи").replace("FC Internazionale Milano","Интер").replace("ACF Fiorentina","Фиорентина").replace("SS Lazio","Лацио").replace("AS Roma","Рома").replace("AC Milan","Милан").replace("Parma Calcio 1913","Парма").replace("Udinese Calcio","Удинезе").replace("Atalanta BC","Аталанта").replace("UC Sampdoria","Сампдория").replace("Cagliari Calcio","Кальяри").replace("Torino FC","Торино").replace("Bologna FC 1909","Болонья").replace("Genoa CFC","Дженоа").replace("Brescia Calcio","Брешиа").replace("Hellas Verona FC","Верона").replace("US Sassuolo Calcio","Сассуоло").replace("SPAL 2013","СПАЛ").replace("US Lecce","Лечче").replace("BV Borussia 09 Dortmund","Боруссия Д").replace("SC Freiburg","Фрайбург").replace("TSV Fortuna 95 Düsseldorf","Фортуна Д.").replace("Bayer 04 Leverkusen","Байер").replace("VfL Wolfsburg","Вольфсбург").replace("Hertha BSC","Герта").replace("FC Bayern München","Бавария").replace("Eintracht Frankfurt","Айнтрахт Ф").replace("TSG 1899 Hoffenheim","Хоффенхайм").replace("1. FC Union Berlin","Унион Б.").replace("RB Leipzig","РБ Лейпциг").replace("FC Schalke 04","Шальке 04").replace("Borussia Mönchengladbach","Боруссия М").replace("SC Paderborn 07","Падерборн").replace("1. FC Köln","Кёльн").replace("SV Werder Bremen","Вердер").replace("1. FSV Mainz 05","Майнц").replace("FC Augsburg","Аугсбург").replace("Liverpool FC","Ливерпуль").replace("Arsenal FC","Арсенал").replace("Brighton & Hove Albion FC","Брайтон Х.").replace("AFC Bournemouth","Борнмут").replace("Everton FC","Эвертон").replace("Manchester City FC","М. Сити").replace("Manchester United FC","М. Юнайтед").replace("Tottenham Hotspur FC","Тоттенхэм").replace("Burnley FC","Бернли").replace("Norwich City FC","Норвич").replace("Sheffield United FC","Шеффилд Ю.").replace("Wolverhampton Wanderers FC","Вулверхэмптон").replace("Leicester City FC","Лестер").replace("Crystal Palace FC","Кристал Пэлас").replace("West Ham United FC","Вест Хэм Ю.").replace("Aston Villa FC","Астон Вилла").replace("Newcastle United FC","Ньюкасл Ю.").replace("Southampton FC","Саутгемптон").replace("Chelsea FC","Челси").replace("Watford FC","Уотфорд").replace("Real Madrid CF","Реал Мадрид").replace("RCD Mallorca","Мальорка").replace("Athletic Club","Атлетик").replace("Real Sociedad de Fútbol","Р. Сосьедад").replace("Valencia CF","Валенсия").replace("CA Osasuna","Осасуна").replace("Granada CF","Гранада").replace("Villarreal CF","Вильярреал").replace("Club Atlético de Madrid","Атлетико").replace("Levante UD","Леванте").replace("Sevilla FC","Севилья").replace("Getafe CF","Хетафе").replace("Real Betis Balompié","Бетис").replace("RCD Espanyol de Barcelona","Эспаньол").replace("Real Valladolid CF","Вальядолид").replace("Deportivo Alavés","Алавес").replace("SD Eibar","Эйбар").replace("FC Barcelona","Барселона").replace("RC Celta de Vigo","Сельта").replace("CD Leganés","Леганес").replace("Olympique Lyonnais","Лион").replace("OGC Nice","Ницца").replace("FC Metz","Мец").replace("Toulouse FC","Тулуза").replace("Paris Saint-Germain FC","ПСЖ").replace("Stade de Reims","Реймс").replace("AS Saint-Étienne","Сент-Этьен").replace("Stade Rennais FC 1901","Ренн").replace("Amiens SC","Амьен").replace("Lille OSC","Лилль").replace("Angers SCO","Анже").replace("Stade Brestois 29","Брест").replace("RC Strasbourg Alsace","Страсбур").replace("Montpellier HSC","Монпелье").replace("FC Nantes","Нант").replace("FC Girondins de Bordeaux","Бордо").replace("Olympique de Marseille","Марсель").replace("Dijon Football Côte d'Or","Дижон").replace("Nîmes Olympique","Ним").replace("AS Monaco FC","Монако")
    		tableTitile+=' <tr>'+
       ' <td>' +name + '</td>'
       + '<td>'+item.goalDifference+'</td>'
       + '<td>'+item.goalsAgainst + '</td>'
       +  '<td>'+item.goalsFor+'</td>'
       + '<td>'+item.lost+'</td>'
        + '<td>'+item.playedGames + '</td>'
       + '<td>'+ item.points + '</td> </tr>';
    	})


    	var tableEnd = '</tbody> </table> </div>';
    	tableTitile+=tableEnd;
    	
    	$(this.container).html(tableTitile)
	},

	this.getData = function(){
		
		var req = new RequestSender(server + "competitions/"+ this.teamCode +"/standings",
			method,null,header,self)
		req.send()
	}
}


